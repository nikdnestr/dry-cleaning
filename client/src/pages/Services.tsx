import React, { useEffect, useState } from 'react';
import axios from 'axios';

interface I {
  name: string;
  description: string;
  services: [];
}

export const Services = () => {
  const [data, setData] = useState<[I] | undefined>(undefined);
  useEffect(() => {
    const apiCall = () => {
      axios
        .get('http://localhost:4000/api/servicePlaces', {
          headers: { Authorization: `Bearer ${sessionStorage.token}` },
        })
        .then((res) => setData(res.data));
    };
    apiCall();
  }, []);

  return <div>{data ? data[0].name : 'kek'}</div>;
};
