import React, { FC, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Auth } from './pages/Auth';
import { Services } from './pages/Services';

const App: FC = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/services">
            <Services />
          </Route>
          <Route path="/">
            <Auth />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
