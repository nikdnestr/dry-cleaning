import { NextFunction, Request, Response } from "express";
import bcrypt from "bcrypt";
import { ServicePlace } from "./../models/servicePlace.schema";
import { Service } from "./../models/service.schema";
import signJWT from "../helper/signJWT";

export const getAll = (req: Request, res: Response, next: NextFunction) => {
  ServicePlace.find({})
    .then((items) => res.send(items))
    .catch(next);
};

export const getById = (req: Request, res: Response, next: NextFunction) => {
  ServicePlace.findById(req.params.id)
    .then((item) => res.send(item))
    .catch(next);
};

export const getServiceById = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  ServicePlace.findById(req.params.id, {
    services: { _id: req.params.serviceId },
  })
    .then((item) => res.send(item))
    .catch(next);
};

export const createServicePlace = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  ServicePlace.create(req.body)
    .then((item) => res.send(item))
    .catch(next);
};

export const editServicePlace = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!res.locals.user.admin) {
    return res.status(401).json({ message: "Restricted route" });
  }
  ServicePlace.findByIdAndUpdate(req.params.id, req.body).then((item) =>
    ServicePlace.findOne({ _id: req.params.id }).then((item) => res.send(item))
  );
};

export const addService = (req: Request, res: Response) => {
  const { name, description, price } = req.body;
  const i = new Service({
    name,
    description,
    price,
  });
  Service.create(i);
  ServicePlace.findByIdAndUpdate(req.params.id, {
    $push: { services: i._id },
  }).then((item) =>
    ServicePlace.findById(req.params.id).then((item) => res.send(item))
  );
};

export const removeService = (req: Request, res: Response) => {
  if (!res.locals.user.admin) {
    return res.status(401).json({ message: "Restricted route" });
  }
  ServicePlace.findByIdAndUpdate(req.params.id, {
    $pull: { services: { _id: req.params.serviceId } },
  }).then((item) =>
    ServicePlace.findOne({ _id: req.params.id }).then((item) => res.send(item))
  );
};

export const deleteServicePlace = (req: Request, res: Response) => {
  if (!res.locals.user.admin) {
    return res.status(401).json({ message: "Restricted route" });
  }
  ServicePlace.findByIdAndDelete(req.params.id, req.body).then((item) =>
    res.send(item)
  );
};
