import { NextFunction, Request, Response } from 'express';
import { Order } from '../models/order.schema';

const getOrders = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.admin) {
    Order.find({ email: res.locals.user.email })
      .then((items) => res.send(items))
      .catch(next);
    return;
  }
  Order.find({})
    .then((items) => res.send(items))
    .catch(next);
};

const getOrder = (req: Request, res: Response, next: NextFunction) => {
  if (res.locals.user.email !== req.body.email) {
    return res.status(401).json({ message: 'Restricted route' });
  }

  Order.findById(req.params.id)
    .then((item) => res.send(item))
    .catch(next);
};

const makeOrder = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.email) {
    return res.status(401).json({ message: 'Restricted route' });
  }
  Order.create({ email: res.locals.user.email, ...req.body })
    .then((item) => res.send(item))
    .catch(next);
};

const editOrder = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.admin) {
    if (res.locals.user.email !== req.body.email) {
      return res.status(401).json({ message: 'Restricted route' });
    }
    Order.findByIdAndUpdate(req.params.id, req.body)
      .then(() => Order.findById(req.params.id))
      .then((item) => res.send(item))
      .catch(next);
    return;
  }
  Order.findByIdAndUpdate(req.params.id, req.body)
    .then(() => Order.findById(req.params.id))
    .then((item) => res.send(item))
    .catch(next);
};

const removeOrder = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.admin) {
    if (res.locals.user.email !== req.body.email) {
      return res.status(401).json({ message: 'Restricted route' });
    }
    Order.findByIdAndDelete(req.params.id).then((item) => res.send(item));
    return;
  }
  Order.findByIdAndDelete(req.params.id).then((item) => res.send(item));
};

export { getOrders, getOrder, makeOrder, editOrder, removeOrder };
