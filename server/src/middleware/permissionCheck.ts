import { Request, Response, NextFunction } from "express";

export const adminCheck = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.admin) {
    return res.status(401).json({ message: "Restricted route" });
  }
  next();
};

export const ownerCheck = (req: Request, res: Response, next: NextFunction) => {
  if (!res.locals.user.admin) {
    if (res.locals.user.email !== req.body.email) {
      return res
        .status(401)
        .json({ message: "Restricted route", stuff: res.locals });
    }
  }
  next();
};
