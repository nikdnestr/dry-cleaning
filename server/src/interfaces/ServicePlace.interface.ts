import { Document } from "mongoose";
import { IService } from "./";

export interface IServicePlace extends Document {
  name: string;
  description: string;
  services: string;
}
