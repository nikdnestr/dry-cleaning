import { IOrder } from "./Order.interface";
import { IService } from "./Service.interface";
import { IServicePlace } from "./ServicePlace.interface";
import { IUser } from "./User.interface";

export { IOrder, IService, IServicePlace, IUser };
