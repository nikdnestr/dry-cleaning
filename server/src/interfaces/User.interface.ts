import { Document } from "mongoose";

export interface IUser extends Document {
  email: string;
  password: string;
  balance: number;
  admin: boolean;
  createdAt: Date;
  updatedAt: Date;
}
