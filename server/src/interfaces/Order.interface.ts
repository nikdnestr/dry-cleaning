import { Document } from "mongoose";

export interface IOrder extends Document {
  name: string;
  service: string;
  price: number;
}
