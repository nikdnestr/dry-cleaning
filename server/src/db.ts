import config from './config';
import mongoose from 'mongoose';

export const dbConnection = async () => {
  try {
    await mongoose.connect(config.db);
    console.log('Connected to database');
  } catch (error) {
    console.log(error);
    console.log('Could not connect to database');
  }
};
