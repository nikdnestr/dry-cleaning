import dotenv from 'dotenv';

dotenv.config();

const HOSTNAME = process.env.HOSTNAME || 'localhost';
const PORT = process.env.PORT || 4000;
const DB = process.env.DB || 'mongodb://localhost/dry-cleaning';
const JWT_TIME = process.env.JWT_TIME || 3600;
const JWT_SECRET = process.env.HOSTNAME || 'verysecretindeed';
const JWT_ISSUER = process.env.JWT_SECRET || 'issuer';

const config = {
  hostname: HOSTNAME,
  port: PORT,
  db: DB,
  token: {
    time: JWT_TIME,
    secret: JWT_SECRET,
    issuer: JWT_ISSUER,
  },
};

export default config;
