import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import Grid from 'gridfs-stream';
import config from './config';
import { user, servicePlace, order, upload } from './router';
import { dbConnection } from './db';

const app = express();

let gfs;
dbConnection();

const conn = mongoose.connection;
conn.once('open', function () {
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('photos');
});

app.use(express.static('public'));
app.use(cors({ origin: 'http://localhost:3000' }));
app.use(express.json());
app.use('/api/users', user);
app.use('/api/servicePlaces', servicePlace);
app.use('/api/orders', order);
app.use('/api/upload', upload);

app.listen(config.port, () => {
  console.log(`Up on localhost:${config.port}`);
});
