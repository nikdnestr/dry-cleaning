import express from "express";
import {
  getAll,
  getById,
  getServiceById,
  createServicePlace,
  editServicePlace,
  addService,
  removeService,
  deleteServicePlace,
} from "../controllers/servicePlace.controller";
import { extractJWT } from "../middleware/extractJWT";
import { ownerCheck } from "../middleware/permissionCheck";

export const router = express.Router();

router.get("/", extractJWT, getAll);
router.get("/:id", extractJWT, getById);
router.get("/:id/:serviceId", extractJWT, getServiceById);
router.post("/", extractJWT, ownerCheck, createServicePlace);
router.put("/:id", extractJWT, editServicePlace);
router.patch("/:id", extractJWT, addService);
router.delete("/:id/:serviceId", extractJWT, removeService);
router.delete("/:id", extractJWT, deleteServicePlace);
