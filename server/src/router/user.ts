import express from 'express';
import {
  signup,
  login,
  changePassword,
  changeBalance,
  getAllUsers,
  removeUser,
} from '../controllers/user.controller';
import { extractJWT } from '../middleware/extractJWT';
import { adminCheck, ownerCheck } from '../middleware/permissionCheck';

export const router = express.Router();

router.get('/', extractJWT, ownerCheck, getAllUsers);
router.post('/signup', signup);
router.post('/login', login);
router.put('/changePassword', extractJWT, ownerCheck, changePassword);
router.patch('/changeBalance', extractJWT, ownerCheck, changeBalance);
router.delete('/', extractJWT, ownerCheck, removeUser);
