import { router as servicePlace } from './servicePlace';
import { router as user } from './user';
import { router as order } from './order';
import { router as upload } from './upload';

export { servicePlace, user, order, upload };
