import express from 'express';
import { NextFunction, Request, Response } from 'express';
import upload from '../middleware/fileUpload';
import { extractJWT } from '../middleware/extractJWT';

export const router = express.Router();

router.post(
  '/up',
  upload.single('file'),
  (req: Request, res: Response, next: NextFunction) => {
    if (req.file === undefined) return res.send('you must select a file.');
    const imgUrl = `http://localhost:8080/file/${req.file.filename}`;
    return res.send(imgUrl);
  }
);
