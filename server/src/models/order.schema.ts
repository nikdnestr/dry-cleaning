import mongoose from 'mongoose';
import { IOrder } from './../interfaces/index';

const Schema = mongoose.Schema;

const OrderSchema = new Schema(
  {
    email: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    service: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      default: 'PENDING',
    },
  },
  { timestamps: true }
);

export const Order = mongoose.model<IOrder>('Order', OrderSchema);
