export const customErrors = {
  // 400
  BAD_REQUEST_USER_REGISTERED: {
    message: {
      en: "User is already registered",
      ru: "Пользователь уже зарегистрирован",
    },
    code: "USER_IS_ALREADY_REGISTERED",
  },
  BAD_REQUEST_USER_ACTIVATED: {
    message: {
      en: "User is already activated",
      ru: "Пользователь уже активирован",
    },
    code: "USER_IS_ALREADY_ACTIVATED",
  },
  USER_IN_REGISTRATION_STATE_ERROR: {
    message: {
      en: "User filled data but no confirmed by sms code",
      ru: "Пользователь заполнил информацию, но не подтвердил смс",
    },
    code: "USER_IN_REGISTRATION_STATE",
  },
  BAD_REQUEST_NO_TOKEN: {
    message: { en: "Token is not present", ru: "Токен отсутствует" },
  },
  BAD_REQUEST_NO_STOCK: {
    message: { en: "Stock count is zero", ru: "Значение Stock равно нулю" },
  },
  BAD_REQUEST_WRONG_PRODUCT_COUNT: {
    message: { en: "Wrong product count", ru: "Неправильное " },
  },

  BAD_REQUEST_NOT_VALID_FILE: {
    message: "Not valid file",
  },

  PHONE_ALREADY_USED_ERROR: {
    code: "REPEAT_VALUES",
    message: "User with such phone registered",
  },
  EMAIL_ALREADY_USED_ERROR: {
    code: "REPEAT_VALUES",
    message: "User with such email registered",
  },
  //401
  UNAUTHORIZED_BAD_TOKEN: {
    message: "Something wrong with token",
  },

  //403
  FORBIDDEN_USER_NOT_CONFIRMED: {
    message: "User is not confirmed",
    code: "USER_IS_NOT_CONFIRMED",
  },

  //403
  INVALID_CODE_ERROR: {
    message: "Code not equals",
    code: "INVALID_CODE_ERROR",
  },
  //429
  CODE_SENDING_LIMIT_ERROR: {
    message: "Password recovery timeout. Try later",
    code: "RECOVERY_TIMEOUT",
  },
  //429
  CODE_CHECKING_LIMIT_ERROR: {
    message: "Code checking limit. Resend code for continue",
    code: "CODE_CHECKING_LIMIT",
  },
  //408
  RECOVERY_TIMEOUT_ERROR: {
    message: "SMS sending limited",
    code: "CODE_SENDING_LIMIT",
  },
  //408
  RECOVERY_INACTIVE_ERROR: {
    message:
      "Recovery inactive. Send request on start recovery password method",
    code: "RECOVERY_INACTIVE",
  },
  //403
  INACTIVE_VERIFICATION_ERROR: {
    message: "Verification inactive. Code already provided. User active.",
    code: "VERIFICATION_INACTIVE",
  },

  // 404
  NOT_FOUND: {
    message: "Record not found",
  },
  //404
  USER_NOT_FOUND: {
    message: "User not found",
    code: "USER_NOT_FOUND",
  },
  ADMIN_NOT_FOUND: {
    message: "Admin not found",
    code: "ADMIN_NOT_FOUND",
  },
  ADMIN_EMAIL_NOT_FOUND: {
    message: "Admin with such email not found",
    code: "ADMIN_EMAIL_NOT_FOUND",
  },
  EMAIL_NOT_FOUND: {
    message: "Email not found",
    code: "EMAIL_NOT_FOUND",
  },
  INVALID_TOKEN: {
    message: "Invalid token",
    code: "INVALID_TOKEN",
  },
  INJURED_TOKEN: {
    message: "Injured token",
    code: "INJURED_TOKEN",
  },
  VALIDATION_ERROR: {
    message: "Validation error",
    code: "VALIDATION_ERROR",
  },
  INVALID_PASSWORD: {
    message: "Invalid password",
    code: "INVALID_PASSWORD",
  },
  PASSWORD_IS_NOT_EQUAL: {
    message: "Old and current password is not equal",
    code: "PASSWORD_IS_NOT_EQUAL",
  },
  CATEGORY_NOT_FOUND: {
    message: "Category not found",
    code: "CATEGORY_NOT_FOUND",
  },
  NOTIFICATION_NOT_FOUND: {
    message: "Notification not found",
    code: "NOTIFICATION_NOT_FOUND",
  },
  COUNTDOWN_NOT_FOUND: {
    message: "Countdown not found",
    code: "COUNTDOWN_NOT_FOUND",
  },
  ACCESS_TO_DELETE_COUNTDOWN_DENIED: {
    message: "The user is not owner of the countdown",
    code: "NOT_OWNER_COUNTDOWN",
  },

  IMAGE_TYPE_ERROR: {
    message: "Image type is not allowed",
    code: "WRONG_IMAGE_TYPE",
  },
};
